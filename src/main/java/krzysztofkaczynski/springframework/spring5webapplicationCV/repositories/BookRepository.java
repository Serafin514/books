package krzysztofkaczynski.springframework.spring5webapplicationCV.repositories;

import krzysztofkaczynski.springframework.spring5webapplicationCV.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book,Long> {
}
