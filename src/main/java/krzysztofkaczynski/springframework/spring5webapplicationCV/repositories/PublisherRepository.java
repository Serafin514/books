package krzysztofkaczynski.springframework.spring5webapplicationCV.repositories;

import krzysztofkaczynski.springframework.spring5webapplicationCV.model.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher,Long> {
}
