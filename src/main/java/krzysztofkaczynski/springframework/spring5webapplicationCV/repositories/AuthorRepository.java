package krzysztofkaczynski.springframework.spring5webapplicationCV.repositories;

import krzysztofkaczynski.springframework.spring5webapplicationCV.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author,Long> {
}
