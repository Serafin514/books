package krzysztofkaczynski.springframework.spring5webapplicationCV;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring5webapplicationCvApplication {

	public static void main(String[] args) {
		SpringApplication.run(Spring5webapplicationCvApplication.class, args);
	}

}
