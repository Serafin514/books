package krzysztofkaczynski.springframework.spring5webapplicationCV.bootstrap;

import krzysztofkaczynski.springframework.spring5webapplicationCV.model.Author;
import krzysztofkaczynski.springframework.spring5webapplicationCV.model.Book;
import krzysztofkaczynski.springframework.spring5webapplicationCV.model.Publisher;
import krzysztofkaczynski.springframework.spring5webapplicationCV.repositories.AuthorRepository;
import krzysztofkaczynski.springframework.spring5webapplicationCV.repositories.BookRepository;
import krzysztofkaczynski.springframework.spring5webapplicationCV.repositories.PublisherRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository,PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {

        Publisher publisher = new Publisher();
        publisher.setName("cl publisher");

        publisher = publisherRepository.save(publisher);

        Author eric = new Author("Eric", "Clapton");
        Book ericBook = new Book("Eric clapton book","123", publisher);
        ericBook = bookRepository.save(ericBook);
        eric.getBooks().add(ericBook);
        authorRepository.save(eric);

        Author rod = new Author("Rod", "Stewart");
        Book rodBook = new Book("Rod Steward book ","124",publisher);
        rodBook = bookRepository.save(rodBook);
        rod.getBooks().add(rodBook);

        authorRepository.save(rod);
    }
}
